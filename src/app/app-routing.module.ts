import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { LogInComponent } from "./log-in/log-in.component";
import { RegisterComponent } from "./register/register.component";

const routes: Routes = [
  { path: "", component: LogInComponent },
  {path:'tab1',loadChildren:()=>import('./tab1/tab1.module').then(x =>x.Tab1PageModule)},
  { path: "register", component: RegisterComponent },
  {path:'main',loadChildren:()=>import('./tab3/tab3.module').then(x=>x.Tab3PageModule)},
  {
    path: "module",
    loadChildren: () =>
      import("./tabs/tabs.module").then((m) => m.TabsPageModule),
  },
  {
    path: "details/:stateId",
    loadChildren: () =>
      import("./list-page/list-page.module").then((m) => m.ListPagePageModule),
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}

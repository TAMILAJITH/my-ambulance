import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

import { first } from "rxjs/operators";
import { DataService } from "../data.service";

@Component({
  selector: "app-log-in",
  templateUrl: "./log-in.component.html",
  styleUrls: ["./log-in.component.css"],
})
export class LogInComponent implements OnInit {
  loading = false;
  submitted = false;
  returnUrl: any;

  constructor(
    public formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public service: DataService
  ) {}

  ngOnInit() {
    debugger;
    this.service.login().subscribe((x) => {
      this.allValue = x.map((y: any) => {
        this.Id = y.payload.doc.id;
        return {
          Id: y.payload.doc.id,
          FirstName: y.payload.doc.data()["firstName"],
          lastName: y.payload.doc.data()["lastName"],
          username: y.payload.doc.data()["username"],
          password: y.payload.doc.data()["password"],
          role: y.payload.doc.data()["selector"],
        };
      });
      console.log('data',this.allValue)
    });

    this.showAlert = this.service.showAlert;
  }
  Id: any;
  FirstName: any;
  LastName: any;
  UserName: any;
  Password: any;
  allValue: any = [];
  showAlert: boolean;
  getUserName: any;
  ObValue: any;
  localeValue: any;
  singleArray: any;

  public form: FormGroup = this.formBuilder.group({
    username: ["", Validators.required],
    password: ["", Validators.required],
  });

  // convenience getter for easy access to form fields
  get f() {
    return this.form.controls;
  }

  get username() {
    return this.form.get("username");
  }

  get password() {
    return this.form.get("password");
  }

  matchUser() {
    debugger;

    const user = this.allValue.find(
      (x: any) => this.username?.value === x.username
    );

    if (user && user.password != this.password?.value) {
      alert("user not found ");
      return false;
    }

    if(user.role == 'admin'){
      this.service.hasUser = true;
    }else{
      this.service.hasUser =false;
    }
    localStorage.setItem("user", JSON.stringify(user));
    
    
    this.service.user = user;
    
    return true;
  }

  onSubmit() {
    debugger;
    this.submitted = true;

    // reset alerts on submit

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    if (this.matchUser()) {
      debugger;
   
      this.form.reset();
      this.router.navigate(["main"]);

    } else {
      alert("Please enter valid username or password");
    }
    
  }
}

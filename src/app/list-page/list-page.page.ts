import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { DataService } from '../data.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CustomValidator } from '../CustomValidator';
import { ambulance } from '../interface';

@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.page.html',
  styleUrls: ['./list-page.page.scss'],
})
export class ListPagePage implements OnInit {

  id: string;
  detailsData: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private db: AngularFirestore,
    private loadservice: DataService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    // alert("zz");
    debugger;
    this.loadservice.isLoading = true;

    this.route.paramMap.subscribe(params => {

      this.id = params.get('stateId')
      console.log("states", this.id)
      var docRef = this.db.collection("ambulance").doc(this.id);

      docRef.get().toPromise().then((doc) => {
        if (doc.exists) {
          console.log("Document data:", doc.data());
          this.detailsData = doc.data()
          debugger;
          setTimeout(() => {
            this.loadservice.isLoading = false;
          }, 0);
        } else {
          console.log("No such document!");
        }
      }).catch((error) => {
        console.log("Error getting document:", error);
      });
    });

  }


  compareWithFn = (o1, o2) => {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  };

  compareWith = this.compareWithFn;
  customAlertOptions: any = {
    header: 'District',
  };


  showEdit() {
    let datas = JSON.stringify(this.detailsData);
    this.router.navigate(['tabs/addlist', this.id])
  }




}

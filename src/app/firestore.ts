import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

export class Firestore<T = any> {
    private firestore: AngularFirestore
    private tableName: string
    constructor(fireStore: AngularFirestore, tableName: string) {
      this.firestore = fireStore;
      this.tableName = tableName;
    }
  
    getAll() {
      return new Observable<T[]>(obx => {
        this.firestore.collection<T>(this.tableName).valueChanges({ idField: "id" }).subscribe(
          res => {
            obx.next(res)
            obx.complete()
          },
          err => (obx.error(err), obx.complete()),
          () => obx.complete())
      })
    }
  
    getSingle(id: string) {
      return new Observable<T>(obx => {
        this.firestore.collection<T[]>(this.tableName).doc<T>(id).valueChanges().subscribe(
          res => {
            obx.next({ id: id, ...res })
            obx.complete();
          },
          err => { obx.error(err), obx.complete() },
          () => obx.complete())
      })
    }
  
    add(data: T) {
      return new Observable<string>(obx => {
        this.firestore.collection<T>(this.tableName).add(data)
          .then(x => {
            obx.next(x.id)
            obx.complete()
          }).catch(x => {
            obx.error(x)
            obx.complete()
          }).finally(() => {
            obx.complete()
          })
      })
    }
  
    update(id: string, data: T) {
      return new Observable<T>(obx => {
        this.firestore.collection<T>(this.tableName).doc(id).set(data)
          .then(x => {
            obx.next({ id: id, ...data })
            obx.complete()
          }).catch(x => {
            obx.error(x)
            obx.complete()
          }).finally(() => {
            obx.complete()
          })
      })
    }
  
    delete(id: string) {
      return new Observable<boolean>(obx => {
        this.firestore.collection<T>(this.tableName).doc(id).delete()
          .then(x => {
            obx.next(true)
            obx.complete()
          }).catch(x => {
            obx.error(x)
            obx.complete()
          }).finally(() => {
            obx.complete()
          })
      })
    }
  }
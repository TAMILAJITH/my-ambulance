export interface ambulance {
  district: string;
  district_tamil?: string;
  city: string;
  city_tamil?: string;
  area: string;
  description: string;
  address: string;
  mobile: string;
  mobile2: string;
  mobile3: string;
  mobile4: string;
  mobile5: string;
  noOfMobile?: number;
  id?: string;
}


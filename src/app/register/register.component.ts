import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { DataService } from '../data.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
   
  loading = false;
  submitted = false;

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private service: DataService,
      
  ) { }

  ngOnInit() {
     
  }

  form:FormGroup = this.formBuilder.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    username: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(6)]],
    selector:['',Validators.required]
});
  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }
get username(){
  return this.form.get('username');
}
get password(){
  return this.form.get('password')
}

get selector(){
  return this.form.get('selector')
}
 
  onSubmit() {
      this.submitted = true;

      // reset alerts on submit
      

      // stop here if form is invalid
      if (this.form.invalid) {
          return;
      }

      
      this.service.register(this.form.value);
      
      this.service.showAlert=true;
      this.router.navigate(['']);
      
      
          
       
             
  }

}

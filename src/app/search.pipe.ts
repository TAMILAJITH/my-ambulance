import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: "TypingSimpleSearch",
})
export class TypingSimpleSearchPipe implements PipeTransform {
    transform(value: any[], args: string, model: string): any {
        if (!args || !model) return value;

        return value
            .filter((x) => {
                const _1 = (x[args] + "").toLocaleLowerCase().trim();
                const _2 = model.toLocaleLowerCase().trim();
                return _1.includes(_2);
            })
            .sort((a, b) => a[args].localeCompare(b[args]));
    }
}
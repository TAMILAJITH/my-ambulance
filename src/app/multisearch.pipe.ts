import { ReturnStatement } from '@angular/compiler';
import { Pipe, PipeTransform } from "@angular/core";
import { filter } from 'rxjs/operators';
import { ambulance } from './interface';

@Pipe({
  name: "multiSearch",
})
export class MultiSearchPipe implements PipeTransform {
  transform(value: any[], model: string, args: string, args1: string, model2: string): any {
    if (!model || !value || value.length <= 0) return value;
    console.log(model, args, model2, args1)

    if (model.toLocaleLowerCase() === 'All district'.toLocaleLowerCase() || args === null || args === undefined || args === '' || args1 === '' || args1 === undefined || args1 === null) {
      console.log('first')
      if (model.toLocaleLowerCase() === 'All district'.toLocaleLowerCase()) {
        model = ''
      }
      const keys = Object.keys(value[0]);
      const m = model.toLocaleLowerCase().trim()
      return value
        .filter((x) => {
          let flag = false;
          for (let index = 0; index < keys.length; index++) {
            flag = (x[keys[index]] + "").toLocaleLowerCase().includes(m)
            if (flag) break;
          }
          return flag;
        })
        .sort((a, b) => a[keys[0]].localeCompare(b[keys[0]]));
    }
    else if (args1 && args) {
      console.log('2nd')
      return value
        .filter((x) =>
          (x[args] + "").toLocaleLowerCase().trim().includes(model.toLocaleLowerCase().trim()) &&
          (x[args1] + "").toLocaleLowerCase().trim().includes((model2 + "").toLocaleLowerCase().trim())
        )
        .sort((a, b) => a[args].localeCompare(b[args]));
    }
    else {
      console.log('last')
      return value
        .filter((x) => (x[args] + "").toLocaleLowerCase().trim().includes(model.toLocaleLowerCase().trim()))
        .sort((a, b) => a[args].localeCompare(b[args]));
    }
  }
}


@Pipe({
  name: "givenGap",
})
export class LetterGapPipe implements PipeTransform {
  transform(value: string): any {
    if (hasValue(value) && value.length > 6) {
      return value.slice(0, 5) + " " + value.slice(5)
    } else return value;
  }
}



@Pipe({
  name: "multiSearch2",
})
export class MultiSearch2Pipe implements PipeTransform {
  transform(value: ambulance[], model: string, model2: string): any {
    if (!value) return value;
    console.log(model, model2)
    if (model.toLocaleLowerCase().trim() === 'All district'.toLocaleLowerCase().trim() && !hasValue(model2)) {
      console.log('0')
      return sortOrderBy(value, 'city')
    } else if (model.toLocaleLowerCase().trim() === 'All district'.toLocaleLowerCase().trim() && hasValue(model2)) {
      console.log('1')
      return sortOrderBy(value.filter(x =>
        x.city.toLocaleLowerCase().trim().includes(model2.toLocaleLowerCase().trim()) ||
        x.district.toLocaleLowerCase().trim().includes(model2.toLocaleLowerCase().trim())
      ), 'city')
    } else if (hasValue(model) && hasValue(model2)) {
      console.log('2')
      return sortOrderBy(value
        .filter(
          x => x.district.toLocaleLowerCase().trim().includes(model.toLocaleLowerCase().trim())
            && x.city.toLocaleLowerCase().trim().includes(model2.toLocaleLowerCase().trim())
        ), 'city')
    } else if (hasValue(model) && !hasValue(model2)) {
      console.log('3')
      return sortOrderBy(value
        .filter(
          x => x.district.toLocaleLowerCase().trim().includes(model.toLocaleLowerCase().trim())
        ), 'city')
    } else {
      console.log('4')
      return sortOrderBy(value, 'city')
    }
  }
}

export function hasValue(value) {
  if (value === null || value === undefined || value === '')
    return false
  else return true
}


export function sortOrderBy(array: ambulance[], key: string) {
  var sortedList = array.sort(function (x, y) {
    var firstKey = x[key];
    var secKey = y[key];
    var comparison = 0;
    if (firstKey > secKey)
      comparison = 1
    if (firstKey < secKey)
      comparison = -1
    return comparison;
  })

  return sortedList
}
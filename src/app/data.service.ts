import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { ambulance } from "./interface";
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: "root",
})
export class DataService {
  private data = [];

  user: any;
  hasUser:boolean;

  constructor(private fire: AngularFirestore) {}

  isLoading: boolean = false;
  showImages: boolean = false;
  showAlert: boolean;
  showOnlyAdmin: boolean = false;
  showMain: boolean = false;
  

  getValue(): any {
    let takeValue: any = [];
    takeValue = JSON.parse(localStorage.getItem("userName") || "[]");
    return takeValue;
  }

  setValue(value: any) {
    let getValue = [];
    getValue.push(value);
    localStorage.setItem("userName", JSON.stringify(getValue));
  }

  login() {
    return this.fire.collection("Ambulance").snapshotChanges();
  }

  register(data) {
    return this.fire.collection("Ambulance").add(data);
  }

  search(value: ambulance[], fieldName: string, searchValue: string) {
    return value
      .filter((x) => {
        const _1 = (x[fieldName] + "").toLocaleLowerCase().trim();
        const _2 = searchValue.toLocaleLowerCase().trim();
        return _1.includes(_2);
      })
      .sort((a, b) => a[fieldName].localeCompare(b[fieldName]));
  }
}

import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { mobiscroll, MbscFormOptions } from '@mobiscroll/angular-lite';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomValidator } from '../CustomValidator';
import { ambulance } from '../interface';
import { Firestore } from '../firestore';
import { firestore } from 'firebase';
import { DataService } from '../data.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})



export class Tab1Page {

  formSettings: MbscFormOptions = {
    theme: 'ios',
    themeVariant: 'light'
  };


  constructor(private db: AngularFirestore,
    public fb: FormBuilder, private router: Router,
    private route: ActivatedRoute, private loadservice: DataService,
  ) {

    this.ambulanceFire = new Firestore(this.db, "ambulance")

    // alert("zz")
    debugger;


    this.route.paramMap.subscribe(params => {

      this.editId = params.get('id')
      if (params.get('id')) {
        this.loadservice.isLoading = true;
        this.isEditable = true;
        this.ambulanceFire.getSingle(this.editId).subscribe(res => {
          this.ambForm.setValue({
            district: res.district,
            address: res.address,
            area: res.area,
            city: res.city,
            description: res.description,
            mobile: res.mobile,
            mobile2: res?.mobile2,
            mobile3: res?.mobile3,
            mobile4: res?.mobile4,
            mobile5: res?.mobile5,
          });
          this.noOfMobile = res?.noOfMobile;
        })

        setTimeout(() => {
          this.loadservice.isLoading = false;
        }, 0);

      }


    });

  }

  ngOnInit() {

  }

  ambulanceCollection: any;
  user_id: any
  submitted: boolean = false
  editDatas: ambulance;
  editId: string;
  isEditable: boolean = false;

  ambulanceFire: Firestore<ambulance>

  noOfMobile: number = 1;

  get val() { return this.ambForm.controls; }

  dist: string[] = [
    'Chennai',
    'Trichey',
    'Madurai',
    'Tirunelveli',
    'Tuticorin',
    'Ariyalur',
    'Chengalpattu',
    'Coimbatore',
    'Cuddalore',
    'Dharmapuri',
    'Dindigul',
    'Erode',
    'Kallakurichi',
    'Kanchipuram',
    'Kanyakumari',
    'Karur',
    'Krishnagiri',
    'Madurai',
    'Nagapattinam',
    'Namakkal',
    'Nilgiris',
    'Perambalur',
    'Pudukkottai',
    'Ramanathapuram',
    'Ranipet',
    'Salem',
    'Sivaganga',
    'Tenkasi',
    'Thanjavur',
    'Theni',
    'Tiruchirappalli',
    'Tirupathur',
    'Tiruppur',
    'Tiruvallur',
    'Tiruvannamalai',
    'Tiruvarur',
    'Vellore',
    'Viluppuram',
    'Virudhunagar',
  ];

  ambForm: FormGroup = this.fb.group({
    district: [null, [Validators.required]],
    address: [null, [Validators.required]],
    area: [null, Validators.required],
    city: [null, [Validators.required]],
    description: [null],
    mobile: [null, [Validators.required, Validators.min(1000000000), Validators.max(9999999999)]],
    mobile2: [null],
    mobile3: [null],
    mobile4: [null],
    mobile5: [null],
  });

  compareWithFn = (o1, o2) => {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  };

  compareWith = this.compareWithFn;
  customAlertOptions: any = {
    header: 'District',
  };


  addMobilenumber() {

    if (this.noOfMobile == 6) {
      alert("Max 5 numbers are added")
      return
    }

    this.noOfMobile += 1;

    if (this.noOfMobile == 2) {
      this.ambForm.get('mobile2').setValidators([Validators.required, Validators.min(1000000000), Validators.max(9999999999)])
      this.ambForm.get('mobile2').updateValueAndValidity();
    }
    if (this.noOfMobile == 3) {
      this.ambForm.get('mobile3').setValidators([Validators.required, Validators.min(1000000000), Validators.max(9999999999)])
      this.ambForm.get('mobile3').updateValueAndValidity();
    }
    if (this.noOfMobile == 4) {
      this.ambForm.get('mobile4').setValidators([Validators.required, Validators.min(1000000000), Validators.max(9999999999)])
      this.ambForm.get('mobile4').updateValueAndValidity();
    }
    if (this.noOfMobile == 5) {
      this.ambForm.get('mobile5').setValidators([Validators.required, Validators.min(1000000000), Validators.max(9999999999)])
      this.ambForm.get('mobile5').updateValueAndValidity();
    }
  }


  submitValues() {

    debugger
    this.submitted = true;
    if (this.ambForm.invalid) {
      alert("invalid form")
      return;
    }

    let submitData: ambulance = {
      district: this.ambForm.value.district,
      address: this.ambForm.value.address,
      area: this.ambForm.value.area,
      city: this.ambForm.value.city,
      description: this.ambForm.value.description,
      mobile: this.ambForm.value.mobile,
      mobile2: this.ambForm.value.mobile2,
      mobile3: this.ambForm.value.mobile3,
      mobile4: this.ambForm.value.mobile4,
      mobile5: this.ambForm.value.mobile5,
      noOfMobile: this.noOfMobile
    }

    if (this.isEditable) {
      this.ambulanceFire.update(this.editId, submitData).subscribe(res => {
        alert("update Successfully")
        this.ambForm.reset();

        this.router.navigate(['details', this.editId])
      })


    }
    else {
      this.ambulanceFire.add(submitData).subscribe(
        res => {
          alert("create successfully");
          this.ambForm.reset();
          this.router.navigate(['tabs/list'])
        },
        (err) => {
          alert("Internet error");
        },
      )



    }


  }

  clearvalue() {
    this.submitted = false;
    this.ambForm.reset();
    this.noOfMobile = 1;
  }



}

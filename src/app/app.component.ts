import { Component } from "@angular/core";

import { Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { DataService } from "./data.service";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { ambulance } from "./interface";
import { Firestore } from "./firestore";
import { AngularFirestore } from "@angular/fire/firestore";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public loadservice: DataService,
    private http: HttpClient,
    private db: AngularFirestore
  ) {
    this.initializeApp();
  }

  IsLoading: boolean;
  isbindroot: boolean = false;

  ngOnInit() {
    debugger;
    this.isbindroot = false;
    setTimeout(() => {
      this.isbindroot = true;
    }, 2000);
    // alert('going to upload')
    // this.uploadBulkData()

    var user = JSON.parse(localStorage.getItem("user"));

    if(user.role == 'admin'){
      this.loadservice.hasUser = true;
    } else {
      this.loadservice.hasUser =false;
    }
    if (user && user.username && user.password) {
      this.loadservice.user = user;
      
    } else {
      this.loadservice.user = user;
      this.loadservice.hasUser = false;
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.show();
    });
  }

  uploadBulkData() {
    const ambulanceFire = new Firestore<ambulance>(this.db, "ambulance");
    this.http.get("assets/data/ambulance.json").subscribe(
      (res: ambulance[]) => {
        res.forEach((amb) => {
          let updateModel = {
            address: amb.address || null,
            area: amb.area || null,
            city: amb.city || null,
            city_tamil: amb.city_tamil || null,
            description: amb.description || null,
            district: amb.district || null,
            district_tamil: amb.district_tamil || null,
            mobile: amb.mobile || null,
            mobile2: amb.mobile2 || null,
            mobile3: amb.mobile3 || null,
            mobile4: amb.mobile4 || null,
            mobile5: amb.mobile5 || null,
          };

          let mobKey = ["mobile", "mobile2", "mobile3", "mobile4", "mobile5"];
          mobKey.forEach((x) => {
            updateModel[x] = parseInt(updateModel[x] + "") || null;
          });

          ambulanceFire.add(amb).subscribe((res) => {
            console.log("added in firebase with id: " + res);
          });
        });
      },
      (err) => {
        console.log(err);
      }
    );
  }
}

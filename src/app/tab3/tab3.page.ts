import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Router, NavigationExtras } from '@angular/router';
import { DataService } from '../data.service';
import { Firestore } from '../firestore';
import { ambulance } from '../interface';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  datas: any;
  innderDatas: any;
  allinnderDatas: any;
  searchOpen: boolean = false;

  ambulanceFire: Firestore<ambulance>

  filterBy: string = null;

  searchValue: string = '';
  citySearchValue: string = "";
  cityKey: string = "";

  dist: string[] = [
    'All district',
    'Chennai',
    'Trichey',
    'Madurai',
    'Tirunelveli',
    'Tuticorin',
    'Ariyalur',
    'Chengalpattu',
    'Coimbatore',
    'Cuddalore',
    'Dharmapuri',
    'Dindigul',
    'Erode',
    'Kallakurichi',
    'Kanchipuram',
    'Kanyakumari',
    'Karur',
    'Krishnagiri',
    'Madurai',
    'Nagapattinam',
    'Namakkal',
    'Nilgiris',
    'Perambalur',
    'Pudukkottai',
    'Ramanathapuram',
    'Ranipet',
    'Salem',
    'Sivaganga',
    'Tenkasi',
    'Thanjavur',
    'Theni',
    'Tiruchirappalli',
    'Tirupathur',
    'Tiruppur',
    'Tiruvallur',
    'Tiruvannamalai',
    'Tiruvarur',
    'Vellore',
    'Viluppuram',
    'Virudhunagar',
  ];

  constructor(
    private db: AngularFirestore,
    private router: Router,
    public loadservice: DataService
  ) {
    this.ambulanceFire = new Firestore(this.db, "ambulance")
    // alert("zz")
    debugger;
    this.getDatas();
this.showForAdmin=this.loadservice.showOnlyAdmin;
this.showUserName =this.loadservice.user.FirstName;
console.log('ds',this.showUserName);
  }
  showForAdmin:boolean=false;
  showMain:boolean;
  showUserName:any;
  search() {
    debugger;
    this.searchValue = "All district";
    this.filterBy = null;
    this.citySearchValue = null
    this.cityKey = null
    setTimeout(() => {
      this.searchOpen = !this.searchOpen
    }, 0);
  }

  searchValueDup = ''
  changedrop(value) {
    if (value == 'All district') {
      this.searchValueDup = ''
      this.filterBy = ""
      this.cityKey = ''
      this.citySearchValue = ''
    } else {
      this.filterBy = "district"
      this.cityKey = ''
      this.citySearchValue = ''
      this.searchValueDup = value
    }
  }

  logOut(){
    localStorage.removeItem('user');
    this.router.navigate(['']);
  }
  searchChangeMethod() {
    if (this.searchValueDup == "" || this.searchValueDup == null) {
      this.filterBy = null;
      this.cityKey = null;
    } else {
      this.filterBy = "district"
      this.cityKey = "city";
    }
  }

  getDatas() {

    this.loadservice.isLoading = true;
    this.datas = this.db.collection('ambulance');
    this.innderDatas = this.datas.snapshotChanges().pipe(
      map(actions => {
        debugger
        return (actions as any[]).map(a => {
          debugger
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };

        });
      })
    );
    this.innderDatas.subscribe(x => {
      // alert("dddd")
      debugger
      console.log("district", x)
      this.allinnderDatas = x
      setTimeout(() => {
        this.loadservice.isLoading = false;
      }, 0);
    }
    )


  }

  navigate(data) {
    this.router.navigate(['/details', data.id])
  }

  deleteitem(id) {
    // alert("dlt")
    this.ambulanceFire.delete(id).subscribe(res => {
    })
  }

  navgatetoAdd() {
    this.router.navigate(['tabs/addlist'])
  }


}

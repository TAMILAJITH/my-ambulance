import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab3Page } from './tab3.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { Tab3PageRoutingModule } from './tab3-routing.module'
import { TypingSimpleSearchPipe } from '../search.pipe';
import { LetterGapPipe, MultiSearch2Pipe, MultiSearchPipe } from '../multisearch.pipe';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    Tab3PageRoutingModule,
    RouterModule.forChild([{ path: '', component: Tab3Page }]),
  ],
  declarations: [
    Tab3Page,
    TypingSimpleSearchPipe,
    MultiSearchPipe,
    MultiSearch2Pipe,
    LetterGapPipe
  ]
})
export class Tab3PageModule { }

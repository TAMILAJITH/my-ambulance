import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Tab3Page } from './tab3.page';
import {Tab1PageModule} from '../tab1/tab1.module';
const routes: Routes = [
  {
    path: '',
    component: Tab3Page,
  },
  {path:'tab1',loadChildren:()=>import('../tab1/tab1.module').then(x =>x.Tab1PageModule)}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab3PageRoutingModule {}

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDRLcNhfSsG4mCp1_vI6wZnNxlnkZshK1g",
    authDomain: "tmmk-ambulance.firebaseapp.com",
    databaseURL: "https://tmmk-ambulance.firebaseio.com",
    projectId: "tmmk-ambulance",
    storageBucket: "tmmk-ambulance.appspot.com",
    messagingSenderId: "797008712397",
    appId: "1:797008712397:web:705eaf3965efd4eb324f84",
    measurementId: "G-JNYDPS2ZTD"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
